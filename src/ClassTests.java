import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

class ClassTests {
	
	int deckSize = 52, numToDeal = 5;

	@Test
	public void cardTests() {
		Card card = new Card(Suit.Clubs, 0);
		
		assertNotEquals("Diamonds", card.getSuit());
		assertNotEquals("Jack", card.getRank());
	}
	
	@Test
	public void deckTests() {
		Deck deck = new Deck();
		
		List<Card> cards = deck.deal(numToDeal);
		assertEquals(numToDeal, cards.size());
		assertEquals(deckSize - numToDeal, deck.getDeck().size());
	}
	
	@Test
	public void cardPlayerTests() {
		Deck deck = new Deck();
		CardPlayer player = new CardPlayer();
		Card card = new Card(Suit.Hearts, 0);
		
		player.getCards(deck.deal(numToDeal));
		assertEquals(card.getSuit(), player.getHand().get(0).getSuit());
		assertEquals(card.getRank(), player.getHand().get(0).getRank());
	}

}

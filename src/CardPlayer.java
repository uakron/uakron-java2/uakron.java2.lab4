import java.util.ArrayList;
import java.util.List;

public class CardPlayer {
	
	private List<Card> hand = new ArrayList<Card>();
	
	public void getCards(List<Card> newCards) {
		hand.addAll(newCards);
	}
	
	public void showCards() {
		hand.forEach(card -> System.out.println(card.getRank() + " of " + card.getSuit()));
	}
	
	public List<Card> getHand() {
		return hand;
	}
	
}

enum Suit {
	Hearts, Diamonds, Spades, Clubs;
}

public class Card {
	
	private Suit suit;
	private int rank;
	private String[] values = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
	
	public Card(Suit newSuit, int newRank) {
		suit = newSuit;
		rank = newRank;
	}
	
	public String getSuit() {
		return suit.name();
	}
	
	public String getRank() {
		return values[rank];
	}
	
}

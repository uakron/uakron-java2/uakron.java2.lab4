import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
	
	private List<Card> deck = new ArrayList<Card>();
	
	public Deck() {
		int LOWER = 0, UPPER = 12;
		
		for (Suit suit: Suit.values()) {
			for (int rank = LOWER; rank <= UPPER; rank++) {
				deck.add(new Card(suit, rank));
			}
		}
	}
	
	public void shuffle() {
		Collections.shuffle(deck);
	}
	
	public List<Card> deal(int numToDeal) {
		List<Card> cards = new ArrayList<Card>();
		
		for (int i = 0; i < numToDeal; i++)
			cards.add(deck.get(i));
		
		deck.removeAll(cards);
		
		return cards;
	}
	
	public List<Card> getDeck() {
		return deck;
	}
	
}

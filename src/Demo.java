public class Demo {
	
	private static Deck deck = new Deck();
	private static CardPlayer player = new CardPlayer();
	private static int numCards = 5;
	
	public static void main(String[] args) {
		deck.shuffle();
		
		player.getCards(deck.deal(numCards));
		
		player.showCards();
	}
	
}
